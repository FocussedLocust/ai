#include "Matrix3.h"

Matrix3::Matrix3()
{
	m[0][0] = 1;
	m[0][1] = 0;
	m[0][2] = 0;

	m[1][0] = 0;
	m[1][1] = 1;
	m[1][2] = 0;

	m[2][0] = 0;
	m[2][1] = 0;
	m[2][2] = 1;
}

Matrix3::Matrix3(float a_Xx, float a_Xy, float a_Xz, float a_Yx, float a_Yy, float a_Yz, float a_Zx, float a_Zy, float a_Zz)
{
	m[0][0] = a_Xx;
	m[0][1] = a_Xy;
	m[0][2] = a_Xz;

	m[1][0] = a_Yx;
	m[1][1] = a_Yy;
	m[1][2] = a_Yz;

	m[2][0] = a_Zy;
	m[2][1] = a_Zy;
	m[2][2] = a_Zz;
}


//M + M
Matrix3 Matrix3::operator+(const Matrix3 & rhs) const
{
	Matrix3 newMat;
	newMat.m[0][0] = m[0][0] + rhs.m[0][0];
	newMat.m[0][1] = m[0][1] + rhs.m[0][1];
	newMat.m[0][2] = m[0][2] + rhs.m[0][2];

	newMat.m[1][0] = m[1][0] + rhs.m[1][0];
	newMat.m[1][1] = m[1][1] + rhs.m[1][1];
	newMat.m[1][2] = m[1][2] + rhs.m[1][2];

	newMat.m[2][0] = m[2][0] + rhs.m[2][0];
	newMat.m[2][1] = m[2][1] + rhs.m[2][1];
	newMat.m[2][2] = m[2][2] + rhs.m[2][2];
	return Matrix3();
}

//M += M
Matrix3& Matrix3::operator+=(const Matrix3 & rhs)
{
	*this = *this + rhs;
	return *this;
}

//M - M
Matrix3 Matrix3::operator-(const Matrix3 & rhs) const
{
	Matrix3 newMat;
	newMat.m[0][0] = m[0][0] - rhs.m[0][0];
	newMat.m[0][1] = m[0][1] - rhs.m[0][1];
	newMat.m[0][2] = m[0][2] - rhs.m[0][2];

	newMat.m[1][0] = m[1][0] - rhs.m[1][0];
	newMat.m[1][1] = m[1][1] - rhs.m[1][1];
	newMat.m[1][2] = m[1][2] - rhs.m[1][2];

	newMat.m[2][0] = m[2][0] - rhs.m[2][0];
	newMat.m[2][1] = m[2][1] - rhs.m[2][1];
	newMat.m[2][2] = m[2][2] - rhs.m[2][2];
	return newMat;
}

//M -= M
Matrix3& Matrix3::operator-=(const Matrix3 & rhs)
{
	*this = *this - rhs;
	return *this;
}

Matrix3 Matrix3::operator*(const Matrix3 & rhs) const
{
	Matrix3 newMat;
	//row 0, col 0 = dot product of lhs row 0, rhs col 0

	newMat.m[0][0] = (m[0][0] * rhs.m[0][0]) + (m[1][0] * rhs.m[0][1]) + (m[2][0] * rhs.m[0][2]);
	newMat.m[0][1] = (m[0][1] * rhs.m[0][0]) + (m[1][1] * rhs.m[0][1]) + (m[2][1] * rhs.m[0][2]);
	newMat.m[0][2] = (m[0][2] * rhs.m[0][0]) + (m[1][2] * rhs.m[0][1]) + (m[2][2] * rhs.m[0][2]);
																			
	newMat.m[1][0] = (m[0][0] * rhs.m[1][0]) + (m[1][0] * rhs.m[1][1]) + (m[2][0] * rhs.m[1][2]);
																			
	newMat.m[1][1] = (m[0][1] * rhs.m[1][0]) + (m[1][1] * rhs.m[1][1]) + (m[2][1] * rhs.m[1][2]);
	newMat.m[1][2] = (m[0][2] * rhs.m[1][0]) + (m[1][2] * rhs.m[1][1]) + (m[2][2] * rhs.m[1][2]);
																			
	newMat.m[2][0] = (m[0][0] * rhs.m[2][0]) + (m[1][0] * rhs.m[2][1]) + (m[2][0] * rhs.m[2][2]);
	newMat.m[2][1] = (m[0][1] * rhs.m[2][0]) + (m[1][1] * rhs.m[2][1]) + (m[2][1] * rhs.m[2][2]);
	newMat.m[2][2] = (m[0][2] * rhs.m[2][0]) + (m[1][2] * rhs.m[2][1]) + (m[2][2] * rhs.m[2][2]);

	return newMat;
}

Matrix3 & Matrix3::operator*=(const Matrix3 & rhs)
{
	*this = *this * rhs;
	return *this;
}

Vector2 Matrix3::operator*(const Vector2 & rhs)
{
	Vector2 newVec;

	newVec.x = (m[0][0] * rhs.x) + (m[0][1] * rhs.y);
	newVec.y = (m[1][0] * rhs.x) + (m[1][1] * rhs.y);

	return Vector2();
}

Vector3 Matrix3::operator*(const Vector3 & rhs)
{
	Vector3 newVec;

	newVec.x = (m[0][0] * rhs.x) + (m[0][1] * rhs.y) + (m[0][2] * rhs.z);
	newVec.y = (m[1][0] * rhs.x) + (m[1][1] * rhs.y) + (m[1][2] * rhs.z);
	newVec.z = (m[2][0] * rhs.x) + (m[2][1] * rhs.y) + (m[2][2] * rhs.z);

	return Vector3();
}

void Matrix3::setScaleXY(float x, float y)
{
	m[0][0] *= x;
	m[1][1] *= y;
}

void Matrix3::setScaleXYZ(float x, float y, float z)
{
	m[0][0] *= x;
	m[1][1] *= y;
	m[2][2] *= z;
}

void Matrix3::setTranslateXY(float x, float y)
{
	m[0][2] = x;
	m[1][2] = y;
}

void Matrix3::setTranslateXYZ(float x, float y, float z)
{
	m[0][2] = x;
	m[1][2] = y;
	m[2][2] = z;
}

void Matrix3::setRotateX(float rad)
{
	m[1][1] *= cos(rad);
	m[1][2] *= -sin(rad);

	m[2][1] *= sin(rad);
	m[2][2] *= cos(rad);
}

void Matrix3::setRotateY(float rad)
{
	m[0][0] *= cos(rad);
	m[0][2] *= -sin(rad);

	m[2][0] *= sin(rad);
	m[2][2] *= cos(rad);
}

void Matrix3::setRotateZ(float rad)
{
	m[0][0] *= cos(rad);
	m[0][1] *= -sin(rad);

	m[1][0] *= sin(rad);
	m[1][1] *= cos(rad);
}

Matrix3::operator float*()
{
}
