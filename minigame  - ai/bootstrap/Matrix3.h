#pragma once
#include "Vector2.h"
#include "Vector3.h"

class Matrix3
{
public:

	float m[3][3];

	Matrix3();
	Matrix3(float a_Xx, float a_Xy, float a_Xz, float a_Yx, float a_Yy, float a_Yz, float a_Zx, float a_Zy, float a_Zz);
	Matrix3 operator+(const Matrix3 & rhs) const;
	Matrix3& operator+=(const Matrix3 & rhs);
	Matrix3 operator-(const Matrix3 & rhs) const;
	Matrix3& operator-=(const Matrix3 & rhs);

	Matrix3 operator*(const Matrix3 & rhs) const;
	Matrix3& operator*=(const Matrix3 & rhs);

	Vector2 operator*(const Vector2& other);
	Vector3 operator*(const Vector3& other);

	void setScaleXY(float x, float y);
	void setScaleXYZ(float x, float y, float z);

	void setTranslateXY(float x, float y);
	void setTranslateXYZ(float x, float y, float z);

	void setRotateX(float rad);
	void setRotateY(float rad);
	void setRotateZ(float rad);

	explicit operator float*();
};

