#pragma once
#include "Vector3.h"
#include "Vector4.h"

class Matrix4
{
public:
	Matrix4();

	float m[4][4];

	Matrix4(float a_Xx, float a_Xy, float a_Xz, float a_Xw, float a_Yx, float a_Yy, float a_Yz, float a_Yw, float a_Zx, float a_Zy, float a_Zz, float a_Zw, float a_Tx, float a_Ty, float a_Tz, float a_Tw);
	Matrix4 operator+(const Matrix4 & rhs) const;
	Matrix4& operator+=(const Matrix4 & rhs);
	Matrix4 operator-(const Matrix4 & rhs) const;
	Matrix4& operator-=(const Matrix4 & rhs);
	Matrix4 operator*(const Matrix4 & rhs) const;
	Matrix4& operator*=(const Matrix4 & rhs);

	Vector3 operator*(const Vector3& other);
	Vector4 operator*(const Vector4& other);

	void setScaleXYZ(float x, float y, float z);

	void setTranslateXYZ(float x, float y, float z);

	void setRotateX(float rad);
	void setRotateY(float rad);
	void setRotateZ(float rad);

	explicit operator float*();
};

