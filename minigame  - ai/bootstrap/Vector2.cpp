#include "Vector2.h"



Vector2::Vector2()
{
}

Vector2::Vector2(float a_x, float a_y)
{
	x = a_x;
	y = a_y;

}

Vector2 Vector2::operator+(const Vector2& rhs) const
{
	Vector2 result;

	result.x = x + rhs.x;
	result.y = y + rhs.y;

	return result;
}

Vector2 Vector2::operator-(const Vector2& rhs) const
{
	Vector2 result;

	result.x = x - rhs.x;
	result.y = y - rhs.y;

	return result;
}

Vector2 Vector2::operator*(const float rhs) const
{
	Vector2 result;

	result.x = x * rhs;
	result.y = y * rhs;

	return result;
}

Vector2 & Vector2::operator+=(const Vector2& rhs)
{
	x += rhs.x;
	y += rhs.y;

	return *this;
}

Vector2 & Vector2::operator-=(const Vector2 & rhs)
{
	x -= rhs.x;
	y -= rhs.y;

	return *this;
}

Vector2 & Vector2::operator*=(const Vector2& rhs)
{
	x *= rhs.x;
	y *= rhs.y;

	return *this;
}

bool Vector2::operator==(const Vector2& rhs) const
{
	if (x == rhs.x && y == rhs.y)
	{
		return true;
	}

	return false;
}

bool Vector2::operator!=(const Vector2& rhs) const
{
	return !(*this == rhs);
}

Vector2 Vector2::Add(const Vector2& other) const
{
	Vector2 newVec;

	newVec.x = x + other.x;
	newVec.y = y + other.y;

	return newVec;
}

Vector2 Vector2::Subtract(const Vector2& other) const
{
	Vector2 newVec;

	newVec.x = x - other.x;
	newVec.y = y - other.y;

	return newVec;
}

Vector2 Vector2::Multiply(const float other) const
{
	Vector2 newVec;

	newVec.x = x * other;
	newVec.y = y * other;

	return newVec;
}

Vector2 Vector2::Divide(const float other) const
{
	Vector2 newVec;

	newVec.x = x / other;
	newVec.y = y / other;

	return newVec;
}

Vector2 Vector2::Scale(const float other) const
{
	Vector2 newVec;

	newVec.x = x * other;
	newVec.y = y * other;

	return newVec;
}

float Vector2::Dot(const Vector2& rhs) const
{
	float dx = x * rhs.x;
	float dy = y * rhs.y;
	return dx + dy;
}


float Vector2::Magnitude() const
{
	float Mag;
	Mag = sqrt((x*x) + (y*y));
	return Mag;
}

float Vector2::SqrMagnitude() const
{
	return x*x + y*y;
}


void Vector2::Normalise()
{
	float Mag = Magnitude();
	x /= Mag;
	y /= Mag;
}


Vector2 Vector2::Normalised() const
{
	Vector2 copy;
	copy = *this;
	copy.Normalise();
	return copy;
}

Vector2::operator float*()
{
	return &x;
}

float & Vector2::operator[](int index)
{
	float* fptr = (float*)(this);
	return fptr[index];
}

Vector2::~Vector2()
{
}

Vector2 operator*(float lhs, const Vector2 & rhs)
{
	return rhs * lhs;
}
