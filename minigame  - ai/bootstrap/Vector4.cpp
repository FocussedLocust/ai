#include "Vector4.h"

Vector4::Vector4()
{
}

Vector4::Vector4(float a_x, float a_y, float a_z, float a_w)
{
	x = a_x;
	y = a_y;
	z = a_z;
	w = a_w;
}

Vector4::~Vector4()
{
}

Vector4 Vector4::Add(Vector4& other)
{
	Vector4 newVec;

	newVec.x = x + other.x;
	newVec.y = y + other.y;
	newVec.z = z + other.z;

	return newVec;
}

Vector4 Vector4::Subtract(Vector4& other)
{
	Vector4 newVec;

	newVec.x = x - other.x;
	newVec.y = y - other.y;
	newVec.z = z - other.z;

	return newVec;
}

Vector4 Vector4::Scale(float other)
{
	Vector4 newVec;

	newVec.x = x * other;
	newVec.y = y * other;
	newVec.z = z * other;

	return newVec;
}

Vector4 Vector4::Multiply(float other)
{
	Vector4 newVec;

	newVec.x = x * other;
	newVec.y = y * other;
	newVec.z = z * other;

	return newVec;
}

Vector4 Vector4::Divide(float other)
{
	Vector4 newVec;

	newVec.x = x / other;
	newVec.y = y / other;
	newVec.z = z / other;
	newVec.w = w;

	return newVec;
}

Vector4 Vector4::operator+(Vector4& rhs) const
{
	Vector4 result;

	result.w = w + rhs.w;
	result.x = x + rhs.x;
	result.y = y + rhs.y;
	result.z = z + rhs.z;

	return result;
}

Vector4 Vector4::operator-(Vector4& rhs) const
{
	Vector4 result;

	result.w = w - rhs.w;
	result.x = x - rhs.x;
	result.y = y - rhs.y;
	result.z = z - rhs.z;

	return result;
}

Vector4 Vector4::operator*(float rhs) const
{
	Vector4 result;

	result.x = x * rhs;
	result.y = y * rhs;
	result.z = z * rhs;
	result.w = w * rhs;

	return result;
}

Vector4 & Vector4::operator+=(Vector4& rhs)
{
	w += rhs.w;
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;

	return *this;
}

bool Vector4::operator==(Vector4& rhs)
{
	if (w == rhs.w && x == rhs.x && y == rhs.y && z == rhs.z)
	{
		return true;
	}

	return false;
}

bool Vector4::operator!=(Vector4& rhs)
{
	return !(*this == rhs);
}

float Vector4::Magnitude() const
{
	float Mag;
	Mag = sqrt((x*x) + (y*y) + (z*z));
	return Mag;
}

float Vector4::SqrMagnitude() const
{
	return x*x + y*y + z*z;
}

void Vector4::Normalise()
{
	if (x == 0.0f && y == 0.0f && z == 0.0f)
	{
		return;
	}
	
	float Mag = Magnitude();
	x /= Mag;
	y /= Mag;
	z /= Mag;
}

Vector4 Vector4::Normalised() const
{
	Vector4 copy;
	copy = *this;
	copy.Normalise();
	return copy;
}

float Vector4::Dot(const Vector4& rhs) const
{
	float dx = x * rhs.x;
	float dy = y * rhs.y;
	float dz = z * rhs.z;

	return x * rhs.x + y * rhs.y + z * rhs.z;
}

Vector4 Vector4::Cross(const Vector4 & rhs) const
{
	Vector4 newVec;
	newVec.x = y*rhs.z - z*rhs.y;
	newVec.y = z*rhs.x - x*rhs.z;
	newVec.z = x*rhs.y - y*rhs.x;
	newVec.w = w;

	return newVec;

}

Vector4::operator float*()
{
	return &x;
}

float & Vector4::operator[](int index)
{
	float* fptr = (float*)(this);
	return fptr[index];
}

Vector4 operator*(float lhs, const Vector4 & rhs)
{
	return rhs * lhs;
}