#pragma once
#include <math.h>

class Vector4
{
public:
	Vector4();

	Vector4::Vector4(float a_x, float a_y, float a_z, float a_w);

	~Vector4();

	float x;
	float y;
	float z;
	float w;

	Vector4 operator+ (Vector4& rhs) const;
	Vector4 operator- (Vector4& rhs) const;
	Vector4 operator* (float rhs) const;
	Vector4& operator+= (Vector4& rhs);
	bool operator==(Vector4& rhs);
	bool operator!=(Vector4& rhs);

	float Magnitude() const;

	float SqrMagnitude() const;

	void Normalise();

	Vector4 Normalised() const;

	float Dot(const Vector4 & rhs) const;

	Vector4 Cross(const Vector4 & rhs) const;

	//cast to float
	operator float*();

	//subscript operator
	float & operator[](int index);

	Vector4 Add(Vector4& other);
	Vector4 Subtract(Vector4& other);
	Vector4 Multiply(float other);
	Vector4 Divide(float other);
	Vector4 Scale(float other);

};

Vector4 operator*(float lhs, const Vector4& rhs);