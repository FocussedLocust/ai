#include "Matrix2.h"

Matrix2::Matrix2()
{
	m[0][0] = 1;
	m[0][1] = 0;

	m[1][0] = 0;
	m[1][1] = 1;
}

Matrix2::Matrix2(float a_Xx, float a_Xy, float a_Yx, float a_Yy)
{
	m[0][0] = a_Xx;
	m[0][1] = a_Xy;

	m[1][0] = a_Yx;
	m[1][1] = a_Yy;
}


//M + M
Matrix2 Matrix2::operator+(const Matrix2 & rhs) const
{
	Matrix2 newMat;
	newMat.m[0][0] = m[0][0] + rhs.m[0][0];
	newMat.m[0][1] = m[0][1] + rhs.m[0][1];
	newMat.m[1][0] = m[1][0] + rhs.m[1][0];
	newMat.m[1][1] = m[1][1] + rhs.m[1][1];
	return Matrix2();
}

//M += M
Matrix2& Matrix2::operator+=(const Matrix2 & rhs)
{
	*this = *this + rhs;
	return *this;
}

//M - M
Matrix2 Matrix2::operator-(const Matrix2 & rhs) const
{
	Matrix2 newMat;
	newMat.m[0][0] = m[0][0] - rhs.m[0][0];
	newMat.m[0][1] = m[0][1] - rhs.m[0][1];
	newMat.m[1][0] = m[1][0] - rhs.m[1][0];
	newMat.m[1][1] = m[1][1] - rhs.m[1][1];
	return newMat;
}

//M -= M
Matrix2& Matrix2::operator-=(const Matrix2 & rhs)
{
	*this = *this - rhs;
	return *this;
}

//Matrix Multiplication
Matrix2 Matrix2::operator*(const Matrix2 & rhs) const
{
	Matrix2 newMat;
	//row 0, col 0 = dot product of lhs row 0, rhs col 0
	newMat.m[0][0] = (rhs.m[0][0] * m[0][0]) + (rhs.m[0][1] * m[1][0]);
	newMat.m[0][1] = (rhs.m[0][0] * m[0][1]) + (rhs.m[0][1] * m[1][1]);
														    
	newMat.m[1][0] = (rhs.m[1][0] * m[0][0]) + (rhs.m[1][1] * m[1][0]);
	newMat.m[1][1] = (rhs.m[1][0] * m[0][1]) + (rhs.m[1][1] * m[1][1]);

	return newMat;
}

Matrix2 & Matrix2::operator*=(const Matrix2 & rhs)
{
	*this = *this * rhs;
	return *this;
}

Vector2 Matrix2::operator*(const Vector2 & rhs) const
{
	Vector2 newVec;
	
	newVec.x = (m[0][0] * rhs.x) + (m[1][0] * rhs.y);
	newVec.y = (m[0][1] * rhs.x) + (m[1][1] * rhs.y);

	return newVec;


}

void Matrix2::setScaleXY(float x, float y)
{
	m[0][0] = x;
	m[1][1] = y;
}

void Matrix2::setRotate(float rad)
{
	m[0][0] = cos(rad);
	m[0][1] = sin(rad);

	m[1][0] = -sin(rad);
	m[1][1] = cos(rad);
}

Matrix2::operator float*()
{
	return &m[0][0];
}
