#pragma once
#include "Vector2.h"

class Matrix2
{
public: //r x c
	float m[2][2];
					//Xx Xy
					//Yx Yy
					
					//m00 m01
					//m10 m11

	Matrix2();
	Matrix2(float a_Xx, float a_Xy, float a_Yx, float a_Yy);


	Matrix2 operator+(const Matrix2& rhs) const;
	Matrix2& operator+=(const Matrix2& rhs);
	Matrix2 operator-(const Matrix2& rhs) const;
	Matrix2& operator-=(const Matrix2 & rhs);

	Matrix2 operator*(const Matrix2& rhs) const;
	Matrix2& operator*=(const Matrix2& rhs);

	//Matrix2 * Vector2
	Vector2 operator*(const Vector2& other) const;

	void setScaleXY(float x, float y);
	void setRotate(float rad);

	explicit operator float*();


};