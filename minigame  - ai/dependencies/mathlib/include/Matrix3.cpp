#pragma once
#include "Matrix3.h"

Matrix3::Matrix3()
{
	m[0][0] = 1;
	m[0][1] = 0;
	m[0][2] = 0;

	m[1][0] = 0;
	m[1][1] = 1;
	m[1][2] = 0;

	m[2][0] = 0;
	m[2][1] = 0;
	m[2][2] = 1;
}

Matrix3::Matrix3(float a_Xx, float a_Xy, float a_Xz, float a_Yx, float a_Yy, float a_Yz, float a_Zx, float a_Zy, float a_Zz)
{
	m[0][0] = a_Xx;
	m[0][1] = a_Xy;
	m[0][2] = a_Xz;

	m[1][0] = a_Yx;
	m[1][1] = a_Yy;
	m[1][2] = a_Yz;

	m[2][0] = a_Zx;
	m[2][1] = a_Zy;
	m[2][2] = a_Zz;
}


//M + M
Matrix3 Matrix3::operator+(const Matrix3 & rhs) const
{
	Matrix3 newMat;
	newMat.m[0][0] = m[0][0] + rhs.m[0][0];
	newMat.m[0][1] = m[0][1] + rhs.m[0][1];
	newMat.m[0][2] = m[0][2] + rhs.m[0][2];

	newMat.m[1][0] = m[1][0] + rhs.m[1][0];
	newMat.m[1][1] = m[1][1] + rhs.m[1][1];
	newMat.m[1][2] = m[1][2] + rhs.m[1][2];

	newMat.m[2][0] = m[2][0] + rhs.m[2][0];
	newMat.m[2][1] = m[2][1] + rhs.m[2][1];
	newMat.m[2][2] = m[2][2] + rhs.m[2][2];
	return Matrix3();
}

//M += M
Matrix3& Matrix3::operator+=(const Matrix3 & rhs)
{
	*this = *this + rhs;
	return *this;
}

//M - M
Matrix3 Matrix3::operator-(const Matrix3 & rhs) const
{
	Matrix3 newMat;
	newMat.m[0][0] = m[0][0] - rhs.m[0][0];
	newMat.m[0][1] = m[0][1] - rhs.m[0][1];
	newMat.m[0][2] = m[0][2] - rhs.m[0][2];

	newMat.m[1][0] = m[1][0] - rhs.m[1][0];
	newMat.m[1][1] = m[1][1] - rhs.m[1][1];
	newMat.m[1][2] = m[1][2] - rhs.m[1][2];

	newMat.m[2][0] = m[2][0] - rhs.m[2][0];
	newMat.m[2][1] = m[2][1] - rhs.m[2][1];
	newMat.m[2][2] = m[2][2] - rhs.m[2][2];
	return newMat;
}

//M -= M
Matrix3& Matrix3::operator-=(const Matrix3 & rhs)
{
	*this = *this - rhs;
	return *this;
}

Matrix3 Matrix3::operator*(const Matrix3 & rhs) const
{
	Matrix3 newMat;
	//row 0, col 0 = dot product of lhs row 0, rhs col 0
	newMat.m[0][0] = (rhs.m[0][0] * m[0][0]) + (rhs.m[0][1] * m[1][0]) + (rhs.m[0][2] * m[2][0]);
	newMat.m[0][1] = (rhs.m[0][0] * m[0][1]) + (rhs.m[0][1] * m[1][1]) + (rhs.m[0][2] * m[2][1]);
	newMat.m[0][2] = (rhs.m[0][0] * m[0][2]) + (rhs.m[0][1] * m[1][2]) + (rhs.m[0][2] * m[2][2]);

	newMat.m[1][0] = (rhs.m[1][0] * m[0][0]) + (rhs.m[1][1] * m[1][0]) + (rhs.m[1][2] * m[2][0]);
	newMat.m[1][1] = (rhs.m[1][0] * m[0][1]) + (rhs.m[1][1] * m[1][1]) + (rhs.m[1][2] * m[2][1]);
	newMat.m[1][2] = (rhs.m[1][0] * m[0][2]) + (rhs.m[1][1] * m[1][2]) + (rhs.m[1][2] * m[2][2]);

	newMat.m[2][0] = (rhs.m[2][0] * m[0][0]) + (rhs.m[2][1] * m[1][0]) + (rhs.m[2][2] * m[2][0]);
	newMat.m[2][1] = (rhs.m[2][0] * m[0][1]) + (rhs.m[2][1] * m[1][1]) + (rhs.m[2][2] * m[2][1]);
	newMat.m[2][2] = (rhs.m[2][0] * m[0][2]) + (rhs.m[2][1] * m[1][2]) + (rhs.m[2][2] * m[2][2]);
	return newMat;
}

Matrix3 & Matrix3::operator*=(const Matrix3 & rhs)
{
	*this = *this * rhs;
	return *this;
}

Vector2 Matrix3::operator*(const Vector2 & rhs) const
{
	Vector2 newVec;

	newVec.x = (m[0][0] * rhs.x) + (m[0][1] * rhs.y);
	newVec.y = (m[1][0] * rhs.x) + (m[1][1] * rhs.y);

	return Vector2();
}

Vector3 Matrix3::operator*(const Vector3 & rhs) const
{
	Vector3 newVec;

	newVec.x = (m[0][0] * rhs.x) + (m[1][0] * rhs.y) + (m[2][0] * rhs.z);
	newVec.y = (m[0][1] * rhs.x) + (m[1][1] * rhs.y) + (m[2][1] * rhs.z);
	newVec.z = (m[0][2] * rhs.x) + (m[1][2] * rhs.y) + (m[2][2] * rhs.z);

	return newVec;
}

void Matrix3::setScaleXY(float x, float y)
{
	m[0][0] = x;
	m[1][1] = y;
}

void Matrix3::setScaleXYZ(float x, float y, float z)
{
	m[0][0] = x;
	m[1][1] = y;
	m[2][2] = z;
}

void Matrix3::setTranslateXY(float x, float y)
{
	m[0][2] = x;
	m[1][2] = y;
}

void Matrix3::setTranslateXYZ(float x, float y, float z)
{
	m[0][2] = x;
	m[1][2] = y;
	m[2][2] = z;
}

void Matrix3::setRotateX(float rad)
{
	m[1][1] = cos(rad);
	m[1][2] = sin(rad);

	m[2][1] = -sin(rad);
	m[2][2] = cos(rad);
}

void Matrix3::setRotateY(float rad)
{
	m[0][0] = cos(rad);
	m[0][2] = -sin(rad);

	m[2][0] = sin(rad);
	m[2][2] = cos(rad);
}

void Matrix3::setRotateZ(float rad)
{
	m[0][0] = cos(rad);
	m[0][1] = sin(rad);

	m[1][0] = -sin(rad);
	m[1][1] = cos(rad);
}

Matrix3::operator float*()
{
	return &m[0][0];
}

Vector3& Matrix3::operator [](int index)
{
	return *((Vector3*)m[index]);
}

Matrix3 Matrix3::MatofMin()
{
	Matrix3 minMat;

	minMat.m[0][0] = (m[1][1] * m[2][2]) - (m[1][2] * m[2][1]);
	minMat.m[0][1] = (m[1][0] * m[2][2]) - (m[1][2] * m[2][0]);
	minMat.m[0][2] = (m[1][0] * m[2][1]) - (m[1][1] * m[2][0]);

	minMat.m[1][0] = (m[0][1] * m[2][2]) - (m[2][1] * m[0][2]);
	minMat.m[1][1] = (m[0][0] * m[2][2]) - (m[0][2] * m[2][0]);
	minMat.m[1][2] = (m[0][0] * m[2][1]) - (m[2][0] * m[0][1]);

	minMat.m[2][0] = (m[0][1] * m[1][2]) - (m[1][1] * m[0][2]);
	minMat.m[2][1] = (m[0][0] * m[1][2]) - (m[1][0] * m[0][2]);
	minMat.m[2][2] = (m[0][0] * m[1][1]) - (m[1][0] * m[0][1]);

	return minMat;
}

Matrix3 Matrix3::CofactorMatrix()
{
	Matrix3 CofatMat = MatofMin();
	CofatMat[0][1] *= -1;
	CofatMat[1][0] *= -1;
	CofatMat[1][2] *= -1;
	CofatMat[2][1] *= -1;

	return CofatMat;
}

Matrix3 Matrix3::AdjugateMatrix()
{
	Matrix3 CofatMat = CofactorMatrix();
	Matrix3 AdjMat = CofatMat;
	AdjMat[0][1] = CofatMat[1][0];
	AdjMat[1][0] = CofatMat[0][1];
	
	AdjMat[0][2] = CofatMat[2][0];
	AdjMat[2][0] = CofatMat[0][2];
	
	AdjMat[2][1] = CofatMat[1][2];
	AdjMat[1][2] = CofatMat[2][1];

	return AdjMat;
}

Matrix3 Matrix3::InvertedMatrix()
{
	Matrix3 minMat = MatofMin();
	Matrix3 adjMat = AdjugateMatrix();

	float deter = (m[0][0] * minMat[0][0]) - (m[0][1] * minMat[0][1]) + (m[0][2] * minMat[0][2]);

	Matrix3 inverse = adjMat * (1.0f / deter);

	return inverse;
}

Matrix3 Matrix3::operator*(const float rhs) const
{
	Matrix3 newMat;

	newMat.m[0][0] = m[0][0] * rhs;
	newMat.m[0][1] = m[0][1] * rhs;
	newMat.m[0][2] = m[0][2] * rhs;

	newMat.m[1][0] = m[1][0] * rhs;
	newMat.m[1][1] = m[1][1] * rhs;
	newMat.m[1][2] = m[1][2] * rhs;

	newMat.m[2][0] = m[2][0] * rhs;
	newMat.m[2][1] = m[2][1] * rhs;
	newMat.m[2][2] = m[2][2] * rhs;
	return newMat;
}