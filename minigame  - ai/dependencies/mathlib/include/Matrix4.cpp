#pragma once
#include "Matrix4.h"


Matrix4::Matrix4()
{
	m[0][0] = 1;
	m[0][1] = 0;
	m[0][2] = 0;
	m[0][3] = 0;

	m[1][0] = 0;
	m[1][1] = 1;
	m[1][2] = 0;
	m[1][3] = 0;

	m[2][0] = 0;
	m[2][1] = 0;
	m[2][2] = 1;
	m[2][3] = 0;

	m[3][0] = 0;
	m[3][1] = 0;
	m[3][2] = 0;
	m[3][3] = 1;



}

Matrix4::Matrix4(float a_Xx, float a_Xy, float a_Xz, float a_Xw, float a_Yx, float a_Yy, float a_Yz, float a_Yw, float a_Zx, float a_Zy, float a_Zz, float a_Zw, float a_Tx, float a_Ty, float a_Tz, float a_Tw)
{
	m[0][0] = a_Xx;
	m[0][1] = a_Xy;
	m[0][2] = a_Xz;
	m[0][3] = a_Xw;

	m[1][0] = a_Yx;
	m[1][1] = a_Yy;
	m[1][2] = a_Yz;
	m[1][3] = a_Yw;

	m[2][0] = a_Zx;
	m[2][1] = a_Zy;
	m[2][2] = a_Zz;
	m[2][3] = a_Zw;

	m[3][0] = a_Tx;
	m[3][1] = a_Ty;
	m[3][2] = a_Tz;
	m[3][3] = a_Tw;


}


//M + M
Matrix4 Matrix4::operator+(const Matrix4 & rhs) const
{
	Matrix4 newMat;
	newMat.m[0][0] = m[0][0] + rhs.m[0][0];
	newMat.m[0][1] = m[0][1] + rhs.m[0][1];
	newMat.m[0][2] = m[0][2] + rhs.m[0][2];
	newMat.m[0][3] = m[0][3] + rhs.m[0][3];

	newMat.m[1][0] = m[1][0] + rhs.m[1][0];
	newMat.m[1][1] = m[1][1] + rhs.m[1][1];
	newMat.m[1][2] = m[1][2] + rhs.m[1][2];
	newMat.m[1][3] = m[1][3] + rhs.m[1][3];

	newMat.m[2][0] = m[2][0] + rhs.m[2][0];
	newMat.m[2][1] = m[2][1] + rhs.m[2][1];
	newMat.m[2][2] = m[2][2] + rhs.m[2][2];
	newMat.m[2][3] = m[2][3] + rhs.m[2][3];

	newMat.m[3][0] = m[3][0] + rhs.m[3][0];
	newMat.m[3][1] = m[3][1] + rhs.m[3][1];
	newMat.m[3][2] = m[3][2] + rhs.m[3][2];
	newMat.m[3][3] = m[3][3] + rhs.m[3][3];

	return Matrix4();
}

//M += M
Matrix4& Matrix4::operator+=(const Matrix4 & rhs)
{
	*this = *this + rhs;
	return *this;
}

//M - M
Matrix4 Matrix4::operator-(const Matrix4 & rhs) const
{
	Matrix4 newMat;
	newMat.m[0][0] = m[0][0] + rhs.m[0][0];
	newMat.m[0][1] = m[0][1] + rhs.m[0][1];
	newMat.m[0][2] = m[0][2] + rhs.m[0][2];
	newMat.m[0][3] = m[0][3] + rhs.m[0][3];

	newMat.m[1][0] = m[1][0] + rhs.m[1][0];
	newMat.m[1][1] = m[1][1] + rhs.m[1][1];
	newMat.m[1][2] = m[1][2] + rhs.m[1][2];
	newMat.m[1][3] = m[1][3] + rhs.m[1][3];

	newMat.m[2][0] = m[2][0] + rhs.m[2][0];
	newMat.m[2][1] = m[2][1] + rhs.m[2][1];
	newMat.m[2][2] = m[2][2] + rhs.m[2][2];
	newMat.m[2][3] = m[2][3] + rhs.m[2][3];

	newMat.m[3][0] = m[3][0] + rhs.m[3][0];
	newMat.m[3][1] = m[3][1] + rhs.m[3][1];
	newMat.m[3][2] = m[3][2] + rhs.m[3][2];
	newMat.m[3][3] = m[3][3] + rhs.m[3][3];

	return Matrix4();
}

//M -= M
Matrix4& Matrix4::operator-=(const Matrix4 & rhs)
{
	*this = *this - rhs;
	return *this;
}

Matrix4 Matrix4::operator*(const Matrix4 & rhs) const
{
	Matrix4 newMat;
	//row 0, col 0 = dot product of lhs row 0, rhs col 0
	newMat.m[0][0] = (rhs.m[0][0] * m[0][0]) + (rhs.m[0][1] * m[1][0]) + (rhs.m[0][2] * m[2][0]) + (rhs.m[0][3] * m[3][0]);
	newMat.m[0][1] = (rhs.m[0][0] * m[0][1]) + (rhs.m[0][1] * m[1][1]) + (rhs.m[0][2] * m[2][1]) + (rhs.m[0][3] * m[3][1]);
	newMat.m[0][2] = (rhs.m[0][0] * m[0][2]) + (rhs.m[0][1] * m[1][2]) + (rhs.m[0][2] * m[2][2]) + (rhs.m[0][3] * m[3][2]);
	newMat.m[0][3] = (rhs.m[0][0] * m[0][3]) + (rhs.m[0][1] * m[1][3]) + (rhs.m[0][2] * m[2][3]) + (rhs.m[0][3] * m[3][3]);
																											    
	newMat.m[1][0] = (rhs.m[1][0] * m[0][0]) + (rhs.m[1][1] * m[1][0]) + (rhs.m[1][2] * m[2][0]) + (rhs.m[1][3] * m[3][0]);
	newMat.m[1][1] = (rhs.m[1][0] * m[0][1]) + (rhs.m[1][1] * m[1][1]) + (rhs.m[1][2] * m[2][1]) + (rhs.m[1][3] * m[3][1]);
	newMat.m[1][2] = (rhs.m[1][0] * m[0][2]) + (rhs.m[1][1] * m[1][2]) + (rhs.m[1][2] * m[2][2]) + (rhs.m[1][3] * m[3][2]);
	newMat.m[1][3] = (rhs.m[1][0] * m[0][3]) + (rhs.m[1][1] * m[1][3]) + (rhs.m[1][2] * m[2][3]) + (rhs.m[1][3] * m[3][3]);
																											    
	newMat.m[2][0] = (rhs.m[2][0] * m[0][0]) + (rhs.m[2][1] * m[1][0]) + (rhs.m[2][2] * m[2][0]) + (rhs.m[2][3] * m[3][0]);
	newMat.m[2][1] = (rhs.m[2][0] * m[0][1]) + (rhs.m[2][1] * m[1][1]) + (rhs.m[2][2] * m[2][1]) + (rhs.m[2][3] * m[3][1]);
	newMat.m[2][2] = (rhs.m[2][0] * m[0][2]) + (rhs.m[2][1] * m[1][2]) + (rhs.m[2][2] * m[2][2]) + (rhs.m[2][3] * m[3][2]);
	newMat.m[2][3] = (rhs.m[2][0] * m[0][3]) + (rhs.m[2][1] * m[1][3]) + (rhs.m[2][2] * m[2][3]) + (rhs.m[2][3] * m[3][3]);
																											    
	newMat.m[3][0] = (rhs.m[3][0] * m[0][0]) + (rhs.m[3][1] * m[1][0]) + (rhs.m[3][2] * m[2][0]) + (rhs.m[3][3] * m[3][0]);
	newMat.m[3][1] = (rhs.m[3][0] * m[0][1]) + (rhs.m[3][1] * m[1][1]) + (rhs.m[3][2] * m[2][1]) + (rhs.m[3][3] * m[3][1]);
	newMat.m[3][2] = (rhs.m[3][0] * m[0][2]) + (rhs.m[3][1] * m[1][2]) + (rhs.m[3][2] * m[2][2]) + (rhs.m[3][3] * m[3][2]);
	newMat.m[3][3] = (rhs.m[3][0] * m[0][3]) + (rhs.m[3][1] * m[1][3]) + (rhs.m[3][2] * m[2][3]) + (rhs.m[3][3] * m[3][3]);

	return newMat;
}

Matrix4 & Matrix4::operator*=(const Matrix4 & rhs)
{
	*this = *this * rhs;
	return *this;
}

Vector3 Matrix4::operator*(const Vector3 & rhs) const
{
	Vector3 newVec;

	newVec.x = (m[0][0] * rhs.x) + (m[0][1] * rhs.y) + (m[0][2] * rhs.z);
	newVec.y = (m[1][0] * rhs.x) + (m[1][1] * rhs.y) + (m[1][2] * rhs.z);
	newVec.z = (m[2][0] * rhs.x) + (m[2][1] * rhs.y) + (m[2][2] * rhs.z);

	return newVec;
}

Vector4 Matrix4::operator*(const Vector4 & rhs) const
{
	Vector4 newVec;

	newVec.x = (m[0][0] * rhs.x) + (m[1][0] * rhs.y) + (m[2][0] * rhs.z) + (m[3][0] * rhs.w);
	newVec.y = (m[0][1] * rhs.x) + (m[1][1] * rhs.y) + (m[2][1] * rhs.z) + (m[3][1] * rhs.w);
	newVec.z = (m[0][2] * rhs.x) + (m[1][2] * rhs.y) + (m[2][2] * rhs.z) + (m[3][2] * rhs.w);
	newVec.w = (m[0][3] * rhs.x) + (m[1][3] * rhs.y) + (m[2][3] * rhs.z) + (m[3][3] * rhs.w);

	return newVec;
}

void Matrix4::setScaleXYZ(float x, float y, float z)
{
	m[0][0] = x;
	m[1][1] = y;
	m[2][2] = z;
}

void Matrix4::setTranslateXYZ(float x, float y, float z)
{
	m[0][3] = x;
	m[1][3] = y;
	m[2][3] = z;
}

void Matrix4::setRotateX(float rad)
{
	m[1][1] = cos(rad);
	m[1][2] = sin(rad);

	m[2][1] = -sin(rad);
	m[2][2] = cos(rad);
}

void Matrix4::setRotateY(float rad)
{
	m[0][0] = cos(rad);
	m[0][2] = -sin(rad);

	m[2][0] = sin(rad);
	m[2][2] = cos(rad);
}

void Matrix4::setRotateZ(float rad)
{
	m[0][0] = cos(rad);
	m[0][1] = sin(rad);

	m[1][0] = -sin(rad);
	m[1][1] = cos(rad);
}

Matrix4::operator float*()
{
	return &m[0][0];
}

Vector4& Matrix4::operator [](int index)
{
	return *((Vector4*)m[index]);
}
