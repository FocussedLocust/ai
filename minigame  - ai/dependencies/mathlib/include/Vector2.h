#pragma once
#include <math.h>

class Vector2
{
public:
	Vector2();
	Vector2::Vector2(float a_x, float a_y);

	~Vector2();

	float x;
	float y;

	//operator overloading 
	Vector2 operator+ (const Vector2& rhs) const;
	Vector2 operator- (const Vector2& rhs) const;
	Vector2 operator* (const float rhs) const;
	Vector2& operator+= (const Vector2& rhs);
	Vector2& operator-= (const Vector2& rhs);
	Vector2& operator*= (const Vector2& rhs);
	bool operator==(const Vector2& rhs) const;
	bool operator!=(const Vector2& rhs) const;

	//functions with nice names to achieve operations on vectors
	Vector2 Add(const Vector2& other) const;
	Vector2 Subtract(const Vector2& other) const;
	Vector2 Multiply(const float other) const;
	Vector2 Divide(const float other) const;
	Vector2 Scale(const float other) const;

	float dot(const Vector2& rhs) const;

	float magnitude() const;
	float SqrMagnitude() const;
	void normalise();
	Vector2 normalised() const;

	//overload the cast to a float*
	explicit operator float*();

	//subscript operator
	float& operator[](int index);
	
};

Vector2 operator*(float lhs, const Vector2& rhs);