#pragma once
#include "Vector3.h"

Vector3::Vector3()
{
}

Vector3::Vector3(float a_x, float a_y, float a_z)
{
	x = a_x;
	y = a_y;
	z = a_z;
}

Vector3::~Vector3()
{
}

Vector3 Vector3::Add(const Vector3& other) const
{
	Vector3 newVec;

	newVec.x = x + other.x;
	newVec.y = y + other.y;
	newVec.z = z + other.z;

	return newVec;
}

Vector3 Vector3::Subtract(const Vector3& other) const
{
	Vector3 newVec;

	newVec.x = x - other.x;
	newVec.y = y - other.y;
	newVec.z = z - other.z;

	return newVec;
}

Vector3 Vector3::Scale(const float other) const
{
	Vector3 newVec;

	newVec.x = x * other;
	newVec.y = y * other;
	newVec.z = z * other;

	return newVec;
}

float Vector3::magnitude() const
{
	float Mag;
	Mag = sqrt((x*x) + (y*y) + (z*z));
	return Mag;
}

float Vector3::SqrMagnitude() const
{
	return x*x + y*y + z*z;
}

void Vector3::normalise()
{
	float Mag = magnitude();
	x /= Mag;
	y /= Mag;
	z /= Mag;
}

Vector3 Vector3::normalised() const
{
	Vector3 copy;
	copy = *this;
	copy.normalise();
	return copy;
}

float Vector3::dot(const Vector3& rhs) const
{
	float dx = x * rhs.x;
	float dy = y * rhs.y;
	float dz = z * rhs.z;
	return dx + dy + dz;
}

Vector3 Vector3::cross(const Vector3 & rhs) const
{
	Vector3 newVec;
	newVec.x = y*rhs.z - z*rhs.y;
	newVec.y = z*rhs.x - x*rhs.z;
	newVec.z = x*rhs.y - y*rhs.x;

	return newVec;

}

Vector3 Vector3::Multiply(const float other) const
{
	Vector3 newVec;

	newVec.x = x * other;
	newVec.y = y * other;
	newVec.z = z * other;

	return newVec;
}

Vector3 Vector3::Divide(const float other) const
{
	Vector3 newVec;

	newVec.x = x / other;
	newVec.y = y / other;
	newVec.z = z / other;

	return newVec;
}

Vector3 Vector3::operator+(const Vector3& rhs) const
{
	Vector3 result;

	result.x = x + rhs.x;
	result.y = y + rhs.y;
	result.z = z + rhs.z;

	return result;
}

Vector3 Vector3::operator-(const Vector3& rhs) const
{
	Vector3 result;

	result.x = x - rhs.x;
	result.y = y - rhs.y;
	result.z = z - rhs.z;

	return result;
}

Vector3 Vector3::operator*(const float rhs) const
{
	Vector3 result;

	result.x = x * rhs;
	result.y = y * rhs;
	result.z = z * rhs;

	return result;
}

Vector3 & Vector3::operator+=(const Vector3& rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;

	return *this;
}

Vector3 & Vector3::operator-=(const Vector3& rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;

	return *this;
}

Vector3 & Vector3::operator*=(const Vector3& rhs)
{
	x *= rhs.x;
	y *= rhs.y;
	z *= rhs.z;

	return *this;
}

bool Vector3::operator==(const Vector3& rhs) const
{
	if (x == rhs.x && y == rhs.y && z == rhs.z)
	{
		return true;
	}

	return false;
}

bool Vector3::operator!=(const Vector3& rhs)const
{
	return !(*this == rhs);
}

Vector3 operator*(float lhs, const Vector3 & rhs)
{
	return rhs * lhs;
}

Vector3::operator float*()
{
	return &x;
}

float & Vector3::operator[](int index)
{
	float* fptr = (float*)(this);
	return fptr[index];
}