#pragma once
#include <math.h>

class Vector3
{
public:
	Vector3();

	Vector3::Vector3(float a_x, float a_y, float a_z);

	~Vector3();

	float x;
	float y;
	float z;

	Vector3 operator+ (const Vector3& rhs) const;
	Vector3 operator- (const Vector3& rhs) const;
	Vector3 operator* (const float rhs) const;
	Vector3& operator+= (const Vector3& rhs);
	Vector3& operator-= (const Vector3& rhs);
	Vector3& operator*= (const Vector3& rhs);
	bool operator==(const Vector3& rhs) const;
	bool operator!=(const Vector3& rhs) const;

	Vector3 Add(const Vector3& other) const;
	Vector3 Subtract(const Vector3& other) const;
	Vector3 Multiply(const float other) const;
	Vector3 Divide(const float other) const;
	Vector3 Scale(const float other) const;

	float magnitude() const;
	float SqrMagnitude() const;
	void normalise();
	Vector3 normalised() const;

	float dot(const Vector3 & rhs) const;
	Vector3 cross(const Vector3 & rhs) const;

	//overload the cast to a float*
	explicit operator float*();

	//subscript operator
	float& operator[](int index);

};

Vector3 operator*(float lhs, const Vector3& rhs);