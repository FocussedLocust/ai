#include "Agent.h"

#include <math.h>

Agent::Agent()
{
}

Agent::Agent(aie::Texture * a_Texture)
{
	agentTransform = Transform();
	agentTransform.SetPosition(50, 50);
	agentTransform.setRotation(50);

	m_texture = a_Texture;
	m_maxVelocity = 10;
	m_velocity = Vector2(0, 0);

	//gsduaidas
	///fhasifasi

	/*m_position = Vector2(200, 200);
	
	m_maxForce = Vector2(10, 10);
	m_force = Vector2(0, 0);
	*/
}


Agent::~Agent()
{
}

void Agent::Update(float deltaTime)
{
	//TODO: Physics stuff with force, acceleration, velocity etc...
	float drag = 2.0f;
	int speed = 100;
	int mass = 1;

	//Vector2 direction = Vector2(0, 0);
	//Vector2 forceSum = direction * speed;

	//Vector2 acceleration = forceSum * (1.0f / mass);

	Vector2 dampening = (m_velocity * drag) * deltaTime;

	m_velocity += (m_acceleration - dampening);

	agentTransform.Translate(m_velocity * deltaTime);

	Vector2 rotatedVel = m_velocity.normalised();

	agentTransform.setRotation(atan2(-rotatedVel.x, rotatedVel.y));

	m_velocity += m_acceleration;
	m_acceleration.x = 0;
	m_acceleration.y = 0;

}