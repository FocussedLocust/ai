#pragma once
#include "Vector2.h"
#include "Texture.h"
#include "Transform.h"

class IBehaviour;

class Agent
{

public:
	Agent();
	Agent(aie::Texture* a_Texture);

	~Agent();

	Transform agentTransform;

	void Update(float deltaTime);
	
	Vector2 m_maxForce;
	Vector2 m_velocity;
	Vector2 m_force;
	Vector2 m_acceleration;
	float m_maxVelocity;
	aie::Texture* m_texture;

	Vector2 targetPos;
protected:

	IBehaviour* ohBehave;
};

