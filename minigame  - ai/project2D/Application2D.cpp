#include "Application2D.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "Matrix2.h"
#include "Matrix3.h"
#include <iostream>

Application2D::Application2D(int a_sheight, int a_swidth) {
	swidth = a_swidth;
	sheight = a_sheight;
}

Application2D::~Application2D() {

}

bool Application2D::startup() {
	
	m_2dRenderer = new aie::Renderer2D();

	m_texture = new aie::Texture("./textures/numbered_grid.tga");
	m_shipTexture = new aie::Texture("./textures/ship.png");

	m_font = new aie::Font("./font/consolas.ttf", 32);

	m_audio = new aie::Audio("./audio/powerup.wav");

	m_cameraX = 0;
	m_cameraY = 0;
	m_timer = 0;

	globalNode = new Transform();
	
	//player position is called tf
	tf = new Transform();
	//tf2 is the child of the player
	tf2 = new Transform();
	tf2->SetPosition(200.0f, 300.0f);

	tf2->SetParent(tf);

	//player = new sprite(m_shipTexture, 0, 0, Vector2(50, 50));
	//mover = new SteeringAgent(m_shipTexture);
	//fsmAgent = new FSMAgent();

	graphdaddy = new PathFinding::Graph();
	graphdaddy->Setup(tileSize, swidth / tileSize, sheight / tileSize);

	return true;
}

void Application2D::shutdown() {
	
	delete tf;
	delete m_audio;
	delete m_font;
	delete m_texture;
	delete m_shipTexture;
	delete m_2dRenderer;
}

void Application2D::update(float deltaTime) {
	
	// input example
	aie::Input* input = aie::Input::getInstance();
	frameCount++;
	if (frameCount >= 2)
	{
		if (dirtyPath)
		{
			path = graphdaddy->CalculateAPath(graphdaddy->GetNodeAtPosition(endNode), graphdaddy->GetNodeAtPosition(startNode));
			agentPath = path;
			dirtyPath = false;
		}


		if (agentPath.size())
		{
			pathAgent = *agentPath.begin();
			agentPath.pop_front();
		}
		frameCount -= 2;
	}

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();

	Vector2 mousePos = Vector2(input->getMouseX(), input->getMouseY());

	if (input->wasKeyPressed(aie::INPUT_KEY_D))
	{
		PathFinding::GraphNode* node2Del = graphdaddy->GetNodeAtPosition(mousePos);
		if (node2Del->GetData() == startNode)
		{
			graphdaddy->DeleteNode(*node2Del);
			startNode = Vector2(graphdaddy->GetNodeAtPosition(mousePos)->GetData());
		}
		else
		{
			graphdaddy->DeleteNode(*node2Del);
		}	
	}

	if (input->wasKeyPressed(aie::INPUT_KEY_F))
	{
		dirtyPath = true;
	}

	if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_LEFT))
	{
		startNode = Vector2(graphdaddy->GetNodeAtPosition(mousePos)->GetData());
		dirtyPath = true;
	}

	if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_RIGHT))
	{
		endNode = Vector2(graphdaddy->GetNodeAtPosition(mousePos)->GetData());
		dirtyPath = true;
	}


	if (input->wasKeyPressed(aie::INPUT_KEY_S))
	{
		startNode = graphdaddy->GetNodeAtPosition(mousePos)->GetData();
	}

}


void Application2D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();
	graphdaddy->DrawGraph(*m_2dRenderer, 1);
	if (path.size() > 0)
	{
		std::list<Vector2>::iterator pathIter;
		pathIter = path.begin();

		m_2dRenderer->setRenderColour(1, 1, 0);
		Vector2 prevNode(pathIter->x, pathIter->y);
		pathIter++;
		for ( ; pathIter != path.end(); pathIter++)
		{
			m_2dRenderer->drawCircle((*pathIter).x, pathIter->y, 8);
			m_2dRenderer->drawLine(pathIter->x, pathIter->y, prevNode.x, prevNode.y, 8);
			prevNode = Vector2(pathIter->x, pathIter->y);
		}
		pathIter--;
		m_2dRenderer->setRenderColour(1, 0, 0);
		m_2dRenderer->drawCircle(path.begin()->x, path.begin()->y, 12);
		m_2dRenderer->setRenderColour(0, 1, 0);
		m_2dRenderer->drawCircle(pathIter->x, pathIter->y, 12);

		m_2dRenderer->setRenderColour(0, 0, 1);
		m_2dRenderer->drawCircle(pathAgent.x, pathAgent.y, 12);
	}


	// done drawing sprites
	m_2dRenderer->end();
}