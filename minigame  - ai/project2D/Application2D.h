#pragma once

#include "sprite.h"
#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h"
#include "Matrix2.h"
#include "Matrix3.h"
#include "Transform.h"
#include "SteeringAgent.h"
#include "FSMAgent.h"
#include "Graph.h"
#include <list>
#include "GraphNode.h"
//#include "Vector2.h"

class Application2D : public aie::Application {
public:

	Application2D(int a_sheight, int a_swidth);
	virtual ~Application2D();

	virtual bool startup();
	virtual void shutdown();


	virtual void update(float deltaTime);
	virtual void draw();

protected:

	Transform* globalNode;
	Transform* tf;
	Transform* tf2;

	sprite* player;
	SteeringAgent* mover;
	FSMAgent* fsmAgent;

	std::list<Vector2> path;

	int swidth;
	int sheight;

	Vector2 startNode = Vector2(100, 100);
	Vector2 endNode = Vector2(200, 200);

	Vector2 pathAgent = Vector2(150, 150);
	std::list<Vector2> agentPath;

//	Vector2 velocity;
//	Vector2 direction;
	float speed;
	float mass;
	float drag;

	int frameCount = 0;
	int tileSize = 50;
	bool dirtyPath = false;

	aie::Texture*  playerTexture;

	aie::Renderer2D*	m_2dRenderer;
	aie::Texture*		m_texture;
	aie::Texture*		m_shipTexture;
	aie::Font*			m_font;
	aie::Audio*			m_audio;
	PathFinding::Graph* graphdaddy;
	
	float m_cameraX, m_cameraY;
	float m_timer;
};