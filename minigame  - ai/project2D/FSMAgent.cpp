#include "FSMAgent.h"
#include <assert.h>


FSMAgent::FSMAgent()
{
	//start our agent in the patrol state
	currState = AgentState::PATROLLING;
}

FSMAgent::FSMAgent(aie::Texture * a_Texture)
{
	//start our agent in the patrol state
	currState = AgentState::PATROLLING;

	m_texture = a_Texture;
}


FSMAgent::~FSMAgent()
{
}

void FSMAgent::Update(float deltaTime)
{
	//TODO: add deltatime to agents

	//this is where all of our AI's FSM goes
	switch (currState)
	{
	case AgentState::PATROLLING:
		UpdatePatrolState(deltaTime);
		break;
	case AgentState::CHASEPLAYER:
		UpdateChaseState(deltaTime);
		break;
	case AgentState::ATTACKPLAYER:
		UpdateAttackState(deltaTime);
		break;
	case AgentState::SEARCHFORPLAYER:
		UpdateSearchState(deltaTime);
		break;
	default:
		assert(false);
		break;
	}
}

void FSMAgent::UpdatePatrolState(float deltaTime)
{
	//patrol in here
	wanderBehaviour.Update(deltaTime, this);

	//transition to chase
	Vector2 distBetween = agentTransform.GetPosition() - targetPos;

	//uf(
	if (distBetween.SqrMagnitude() < 40000)
	{
		currState = AgentState::CHASEPLAYER;
	}
}

void FSMAgent::UpdateChaseState(float deltaTime)
{
}

void FSMAgent::UpdateAttackState(float deltaTime)
{
}

void FSMAgent::UpdateSearchState(float deltaTime)
{
}
