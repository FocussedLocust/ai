#pragma once
#include "Agent.h"
#include "WanderBehaviour.h"

enum class AgentState
{
	PATROLLING,
	CHASEPLAYER,
	ATTACKPLAYER,
	SEARCHFORPLAYER
};

class FSMAgent : public Agent
{
public:
	FSMAgent();
	FSMAgent(aie::Texture* a_Texture);
	~FSMAgent();

	void Update(float deltaTime);

private:
	AgentState currState;
	WanderBehaviour wanderBehaviour;

	void UpdatePatrolState(float deltaTime);
	void UpdateChaseState(float deltaTime);
	void UpdateAttackState(float deltaTime);
	void UpdateSearchState(float deltaTime);



};

