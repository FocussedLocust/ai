#include "Graph.h"
#include "GraphNode.h"
#include <queue>
#include "GraphEdge.h"
//#include "Vector2.h"
#include "Renderer2D.h"

namespace PathFinding
{
	bool CompareGScore::operator()(GraphNode* n1, GraphNode* n2)
	{
		return n1->gScore > n2->gScore;
	};
	bool CompareFScore::operator()(GraphNode* n1, GraphNode* n2)
	{
		return n1->fScore > n2->fScore;
	};

	Graph::Graph()
	{
	}


	Graph::~Graph()
	{
	}

	void Graph::Setup(int dist, int width, int height)
	{
		for (size_t y = 0; y < height; y++)
		{
			for (size_t x = 0; x < width; x++)
			{
				AddNode(Vector2(x * dist, y * dist));
			}
		}
		std::vector<GraphNode*>::iterator nodeIter = m_nodes.begin();

		for (; nodeIter != m_nodes.end(); nodeIter++)
		{
			std::vector<GraphNode*>::iterator otherNodeIter = nodeIter + 1;

			for (; otherNodeIter != m_nodes.end(); otherNodeIter++)
			{
				
				if (((*nodeIter)->GetData() - (*otherNodeIter)->GetData()).magnitude() <= dist)
				{
					(*nodeIter)->AddEdge(*otherNodeIter, 1);
					(*otherNodeIter)->AddEdge(*nodeIter, 1);
				}
			}
			
		}


	}

	GraphNode * Graph::AddNode(const Vector2 a_pos)
	{
		GraphNode* node =  new GraphNode(a_pos);
		m_nodes.push_back(node);

		return node;
	}
	 
	GraphEdge * Graph::AddEdge(GraphNode & a_start, GraphNode & a_end, const float a_cost = 1)
	{
		return a_start.AddEdge(&a_end, a_cost);
	}

	void Graph::DrawGraph(aie::Renderer2D & a_Renderer, float a_depth)
	{
		std::vector<GraphNode*>::iterator nodeIter = m_nodes.begin();

		a_Renderer.setRenderColour(1, 1, 1);



		for (; nodeIter != m_nodes.end(); nodeIter++)
		{
			GraphNode* currentNode = (*nodeIter);


			//float x = currentNode->GetData().x;
			//float y = currentNode->GetData().y;

			std::vector<GraphEdge*> edges = *(*nodeIter)->GetEdges();
			std::vector<GraphEdge*>::iterator edgeIter = edges.begin();
			for (; edgeIter != edges.end(); edgeIter += 2)
			{
				float col = (10 - (*edgeIter)->GetCost())/10;
				a_Renderer.setRenderColour(col, col, col);
				a_Renderer.drawLine((*nodeIter)->GetData().x, (*nodeIter)->GetData().y,
					(*edgeIter)->GetEnd()->GetData().x, (*edgeIter)->GetEnd()->GetData().y);
			}
			
			a_Renderer.setRenderColour(1, 1, 1);
			a_Renderer.drawCircle((*nodeIter)->GetData().x, (*nodeIter)->GetData().y, 5);
			//a_Renderer.drawCircle(x, y, 50);
			
		}



	}

	GraphNode * Graph::GetNodeAtPosition(Vector2 & a_pos)
	{
		float smallestDist = FLT_MAX;
		GraphNode* closestNode = nullptr;

		std::vector<GraphNode*>::iterator nodeIter = m_nodes.begin();


		for (; nodeIter != m_nodes.end(); nodeIter++)
		{
			GraphNode* currentNode = (*nodeIter);

			float distance = (currentNode->GetData() - a_pos).magnitude();

			if (distance < smallestDist)
			{
				closestNode = currentNode;
				smallestDist = distance;
			}
		}

		return closestNode;

	}

	void Graph::DeleteNode(GraphNode & a_node)
	{
		std::vector<GraphNode*>::iterator nodeIter = m_nodes.begin();

		for (; nodeIter != m_nodes.end(); )
		{
			
			GraphNode* currentNode = (*nodeIter);

			DeleteEdge(*(*nodeIter), a_node);
			
			if (currentNode == &a_node)
			{
				nodeIter = m_nodes.erase(nodeIter);
			}
			else
			{
				nodeIter++;
			}
		}

		delete &a_node;

	}

	void Graph::DeleteEdge(GraphNode & a_start, GraphNode & a_end)
	{
		std::vector<GraphEdge*>* edges = a_start.GetEdges();
		std::vector<GraphEdge*>::iterator edgeIter = edges->begin();

		for ( ; edgeIter != edges->end(); )
		{
			GraphEdge* currentEdge = (*edgeIter);

			if (currentEdge->GetEnd() == &a_end)
			{	
				//delete currentEdge;
				edgeIter = edges->erase(edgeIter);
			}
			else
			{
				edgeIter += 1;
			}
		}


		//edges;
		
		//delete m_nodes;
	}

	std::vector<GraphNode*>* PathFinding::Graph::GetNodes()
	{
		return nullptr;
	}

	int Graph::GetNodesCount() const
	{
		return m_nodes.size();
	}

	void Graph::BreadthFirstSearch(GraphNode & a_node)
	{
		//create a queue to store GraphNode*
		std::queue< GraphNode* > gnQueue;
		//create an integer for order ( purely for demo purposes ) 
		int order = 0;
		//reset in queue and traversed
		std::vector< GraphNode* >::iterator resetIter = m_nodes.begin();
		for ( ; resetIter != m_nodes.end(); resetIter++)
		{
			(*resetIter)->traversed = false;
			(*resetIter)->inQueue = false;
		}
		//add a_start( start node ) to our queue
		//don't forget to set startNode->inQueue to true
		gnQueue.push(&a_node);
		GraphNode* node;
		//while queue is not empty
		while (!gnQueue.empty())
			//get node from front / pop it
			node = gnQueue.front();
			//process the node
			node->inQueue = true;
			//set node->order to our count
			node->order = order;
			//set node->traversed to true
			node->traversed = true;
			//loop over all the nodes edges
			for (int i = 0; i < node->GetEdges()->size(); i++)
			{
				//if the edges end is not in queue or traversed
				if (!node->GetEdges()->at(i)->GetEnd()->inQueue || !node->GetEdges()->at(i)->GetEnd()->traversed)
				{
					//add it to the queue
					gnQueue.push(node->GetEdges()->at(i)->GetEnd());
				}

			}
				

	}

	std::list<Vector2> Graph::CalculateDijkPath(GraphNode * startNode, GraphNode * endNode)
	{
		//reset all nodes back to default values
			//set traversed and isInQueue to false
			//set prevNode to nullptr
			//set GScore to max number for a float

		for (int i = 0; i < m_nodes.size(); i++)
		{
			m_nodes[i]->inQueue = false;
			m_nodes[i]->traversed = false;
			m_nodes[i]->gScore = INT_MAX;
			m_nodes[i]->prevNode = nullptr;
		}

		//create a list of GraphNode* as your queue (see below)
		//create a CompareGScore comparison object (see below)
		
		std::list<GraphNode*> queue;
		CompareGScore comparison;

		//add the startNode to our queue

		queue.push_back(startNode);
		//set startNode's isInQueue to true
		startNode->inQueue = true;
		//set startNode's prevNode to itself
		//startNode->prevNode = startNode;
		//set startNode's GScore to 0
		startNode->gScore = 0;

		GraphNode* currNode;

		int topInd = 0;
		int topVal = INT_MAX;

		

		//loop over queue while it's not empty
		while (!queue.empty())
		{
			//sort our queue by GScore (see below)
			queue.sort(comparison);

			currNode = queue.back();
			//remove the first node from the queue (back)
			queue.pop_back();
			//set isInQueue to false
			currNode->inQueue = false;
			//set traversed to true
			currNode->traversed = true;

			//loop over all of the currNode's edges
			for (int i = 0; i < currNode->edges.size(); i++)
			{
			   //if edges endNode is not traversed
				if (currNode->edges[i]->GetEnd()->traversed == false)
				{
				   //calculate the newGScore based on currNode.GScore + currEdge's cost
				   //if the newGScore is less than endNode's current GScore
					if (currNode->gScore + 1 < currNode->edges[i]->GetEnd()->gScore)//1 should be edge cost
					{
						//set edge.endNode.prevNode to currNode (update the node it came from)
						currNode->edges[i]->GetEnd()->prevNode = currNode;
						//set edge.endNode.GScore to newScore (update it)
						currNode->edges[i]->GetEnd()->gScore = currNode->gScore + 1;

						//if edge.endNode is not in the queue
						if (currNode->edges[i]->GetEnd()->inQueue == false)
						{
							 //set edge.endNode.IsInQueue to true      
							currNode->edges[i]->GetEnd()->inQueue = true;
							 //add edge.endNode to the queue
							queue.push_back(currNode->edges[i]->GetEnd());
						}
					}
				}
			}
		}

		std::list<Vector2> path;

		GraphNode* pathNode;
		
		pathNode = endNode;

		while (pathNode != nullptr)
		{
			path.push_back(pathNode->GetData());
			pathNode = pathNode->prevNode;
		}
		return path;
	}

	std::list<Vector2> Graph::CalculateAPath(GraphNode * startNode, GraphNode * endNode)
	{
		//reset all nodes back to default values
		//set traversed and isInQueue to false
		//set prevNode to nullptr
		//set GScore to max number for a float
		//set FScore to max number for a float
		//set HScore to the distance between this node and our TargetNode(this is our heuristic function)
		for (int i = 0; i < m_nodes.size(); i++)
		{
			m_nodes[i]->inQueue = false;
			m_nodes[i]->traversed = false;
			m_nodes[i]->prevNode = nullptr;
			m_nodes[i]->gScore = INT_MAX;
			m_nodes[i]->fScore = INT_MAX;
			m_nodes[i]->hScore = Vector2(endNode->GetData() - startNode->GetData()).magnitude() ;
		}

		//add the startNode to our queue
		//set startNode's isInQueue to true
		//set startNode's prevNode to itself
		//set startNode's GScore to 0

		std::list<GraphNode*> queue;
		CompareFScore comparison;

		queue.push_back(startNode);
		startNode->inQueue = true;
		startNode->gScore = 0;

		GraphNode* currNode;

		int topInd = 0;
		int topVal = INT_MAX;

		//loop while queue is not empty

		while (!queue.empty())
		{
			//sort our queue by FScore (using our new Comparison functor)
			queue.sort(comparison);

			currNode = queue.back();
			//remove the first node from the queue (back)
			queue.pop_back();

			//set isInQueue to false
			currNode->inQueue = false;
			//set traversed to true
			currNode->traversed = true;

			//if that node is our targetNode
			//break (we found our target!)
			if (currNode == endNode)
			{
				break;
			}

			//loop over all of the currNode's edges
			for (int i = 0; i < currNode->edges.size(); i++)
			{
				//if edges endNode is not traversed
				if (currNode->edges[i]->GetEnd()->traversed == false)
				{
					//calculate the newGScore based on currNode.GScore + currEdge's cost
					//calculate the newFScore based on newGScore + edge.endNode's heuristic
					float gScore = currNode->gScore + currNode->edges[i]->GetCost(); //1 should be edge cost
					float fScore = currNode->gScore + currNode->edges[i]->GetEnd()->hScore;
					
					//if the newFScore is less than edge.endNode's current FScore
					if (currNode->gScore + 1 < currNode->edges[i]->GetEnd()->gScore)//1 should be edge cost
					{
						//set edge.endNode.prevNode to currNode (update the node it came from)
						currNode->edges[i]->GetEnd()->prevNode = currNode;
						//set edge.endNode.GScore to newScore (update it)
						currNode->edges[i]->GetEnd()->gScore = gScore;
						//set edge.endNode.FScore to newFScore
						currNode->edges[i]->GetEnd()->fScore = fScore;

						//if edge.endNode is not in the queue
						if (currNode->edges[i]->GetEnd()->inQueue == false)
						{
							//set edge.endNode.IsInQueue to true      
							currNode->edges[i]->GetEnd()->inQueue = true;
							//add edge.endNode to the queue
							queue.push_back(currNode->edges[i]->GetEnd());
						}
					}
				}
			}
		}


		std::list<Vector2> path;

		GraphNode* pathNode;

		pathNode = endNode;

		while (pathNode != nullptr)
		{
			path.push_back(pathNode->GetData());
			pathNode = pathNode->prevNode;
		}
		return path;
	}



}