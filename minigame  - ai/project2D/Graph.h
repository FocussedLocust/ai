#pragma once
#include "Vector2.h"
#include <vector>
#include <list>
//#include "Renderer2D.h"
namespace aie
{
	class Renderer2D;
}

namespace PathFinding
{
	//class Vector2;
	class GraphNode;
	class GraphEdge;

	class CompareGScore
	{
	public:
		bool operator()(GraphNode* n1, GraphNode* n2);
	};

	class CompareFScore
	{
	public:
		bool operator()(GraphNode* n1, GraphNode* n2);
	};

	class Graph
	{
	public:
		Graph();
		~Graph();

		void Setup(int dist, int width, int height);

		GraphNode* AddNode(const Vector2 a_pos);

		GraphEdge* AddEdge(GraphNode& a_start, GraphNode& a_end, const float a_cost);

		void DrawGraph(aie::Renderer2D& a_Renderer, float a_depth);

		GraphNode* GetNodeAtPosition(Vector2& a_pos);

		void DeleteNode(GraphNode& a_node);

		void DeleteEdge(GraphNode& a_start, GraphNode& a_end);

		std::vector< GraphNode* >* GetNodes();

		int GetNodesCount() const;

		void BreadthFirstSearch(GraphNode& a_node);

		std::list <Vector2> CalculateDijkPath(GraphNode* startNode, GraphNode* endNode);

		std::list <Vector2> CalculateAPath(GraphNode* startNode, GraphNode* endNode);

	private:
		std::vector< GraphNode* > m_nodes;

	};

}