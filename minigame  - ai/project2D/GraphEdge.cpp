#include "GraphEdge.h"

namespace PathFinding
{

	GraphEdge::GraphEdge()
	{
	}

	GraphEdge::GraphEdge(GraphNode * a_start, GraphNode * a_end, const float a_cost)
	{

		start = a_start;
		end = a_end;
		cost = 5;
	}


	GraphEdge::~GraphEdge()
	{

	}

	GraphNode * GraphEdge::GetEnd() const
	{
		return end;
	}
	float GraphEdge::GetCost() const
	{
		return cost;
	}
}