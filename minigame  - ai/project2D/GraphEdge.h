#pragma once

namespace PathFinding
{

	class GraphNode;

	class GraphEdge
	{
	public:
		GraphEdge();
		GraphEdge(GraphNode* a_start, GraphNode* a_end, const float a_cost);
		~GraphEdge();

		GraphNode* GetEnd() const;
		
		float GetCost() const;
	private:
		GraphNode* start;
		GraphNode* end;

		float cost;
	};

}
