#include "GraphNode.h"
#include "GraphEdge.h"

namespace PathFinding
{
	GraphNode::GraphNode()
	{
	}

	GraphNode::GraphNode(Vector2 a_pos)
	{
		data = a_pos;
	}


	GraphNode::~GraphNode()
	{
	}

	GraphEdge * GraphNode::AddEdge(GraphNode * a_end, const float a_cost)
	{
		
		GraphEdge* edge = new GraphEdge(this, a_end, a_cost);
		edges.push_back(edge);
		return edge;
	}

	void GraphNode::SetData(Vector2 a_data)
	{
		data = a_data;
	}

	Vector2 GraphNode::GetData() const
	{
		return data;
	}

	std::vector<GraphEdge*>* GraphNode::GetEdges()
	{
		return &edges;
	}

	int GraphNode::GetEdgesCount() const
	{
		return edges.size();
	}

}

