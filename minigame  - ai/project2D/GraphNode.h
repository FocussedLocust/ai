#pragma once
#include "Vector2.h"
#include <vector>

namespace PathFinding
{

	class GraphEdge;

	class GraphNode
	{
	public:
		GraphNode();
		GraphNode(Vector2 a_pos);
		~GraphNode();

		GraphEdge* AddEdge(GraphNode* a_end, const float a_cost);

		void SetData(Vector2 a_data);

		Vector2 GetData() const;

		std::vector<GraphEdge*>* GetEdges();

		int GetEdgesCount() const;

		//has the node been processed
		bool traversed;
		//iis the node currently in the queue
		bool inQueue;

		//for dijkstra
		float gScore;
		GraphNode* prevNode;

		//for a*
		float hScore; // heuristic
		float fScore; // gScore + hScore

		//order we processed this node
		int order;

		std::vector<GraphEdge*> edges;

	private:
		Vector2 data;


	};

}