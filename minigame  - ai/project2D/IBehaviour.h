#pragma once
#include "Agent.h"

class IBehaviour
{
public:
	
	virtual void Update(float deltaTime, Agent *pAgent) = 0;
};

