#include "SeekForce.h"



SeekForce::SeekForce()
{
}


SeekForce::~SeekForce()
{
}

Vector2 SeekForce::getForce(Agent* pSteeringAgent, Agent* target)
{
	Vector2 V = target->agentTransform.GetPosition() - pSteeringAgent->agentTransform.GetPosition();
	V.normalise();
	//V * pSteeringAgent->m_maxVelocity;

	return V;
}
