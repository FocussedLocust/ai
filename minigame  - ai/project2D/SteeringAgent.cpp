#include "SteeringAgent.h"
#include "SteeringBehaviour.h"


SteeringAgent::SteeringAgent()
{
	//SteeringBehaviour* SBH;
	//ohBehave = SBH;
	//m_behaviours.push_front(SBH)
}

SteeringAgent::SteeringAgent(aie::Texture * a_Texture) : Agent(a_Texture)
{
	ohBehave = new SteeringBehaviour();
}


SteeringAgent::~SteeringAgent()
{
}

void SteeringAgent::Update(float deltaTime)
{
	ohBehave->Update(deltaTime, this);
	Agent::Update(deltaTime);

}

void SteeringAgent::addForce()
{
	m_velocity += m_force;
	if (m_velocity.x > m_maxVelocity)
	{
		m_velocity.x = m_maxVelocity;
	}
	if (m_velocity.y > m_maxVelocity)
	{
		m_velocity.y = m_maxVelocity;
	}
}