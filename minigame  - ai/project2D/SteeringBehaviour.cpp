#include "SteeringBehaviour.h"



SteeringBehaviour::SteeringBehaviour()
{
	Target = new Agent();
}


SteeringBehaviour::~SteeringBehaviour()
{
}

void SteeringBehaviour::Update(float deltaTime, Agent* pAgent)
{
	Target->agentTransform.SetPosition(200, 200);

	pAgent->m_acceleration = SForce->getForce(pAgent, Target);
}