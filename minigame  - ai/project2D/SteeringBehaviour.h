#pragma once
#include "IBehaviour.h"
#include "SteeringForce.h"
#include "SeekForce.h"

class SteeringBehaviour : public IBehaviour
{
public:
	SteeringBehaviour();
	~SteeringBehaviour();

	SeekForce* SForce;
	Agent* Target;

	void Update(float deltaTime, Agent* pAgent);

};

