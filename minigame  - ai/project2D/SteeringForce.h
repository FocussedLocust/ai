#pragma once
#include "SteeringAgent.h"

class SteeringForce
{
public:

	virtual Vector2 getForce(SteeringAgent* pSteeringAgent) = 0;
};

