#include "Transform.h"


Transform::Transform()
{
	position = Vector2(200, 100);
	rotation = 0.0f;
	scale = Vector2(1, 1);
	parent = nullptr;
}


Transform::~Transform()
{
}

void Transform::UpdateTransform()
{

	translationMat.m[2][0] = position.x;
	translationMat.m[2][1] = position.y;
	rotationMat.setRotateZ(rotation);
	scaleMat.setScaleXY(scale.x, scale.y);

	localMatrix = translationMat * rotationMat * scaleMat;
	
	if (parent == nullptr)
	{
		globalMatrix = localMatrix;

	}
	else
	{
		globalMatrix = parent->globalMatrix * localMatrix;
	}
	int i = 5;
	i *= 2;

}

void Transform::Translate(Vector2 translation)
{
	//Matrix3 parentRotationMatrix;
	//parentRotationMatrix.setRotateZ(-parent->rotation);
	position += translation;

	UpdateTransform();
}

void Transform::Scale(float x, float y)
{
	scale.x += x;
	scale.y += y;

	UpdateTransform();
}

void Transform::Rotate(float angle)
{
	rotation += angle;

	UpdateTransform();
	int i = 5;
	i++;
}


void Transform::SetPosition(float x, float y)
{
	position.x = x;
	position.y = y;

	UpdateTransform();
}

void Transform::SetScale(float x, float y)
{
	scale.x = x;
	scale.y = y;

	UpdateTransform();
}

void Transform::setRotation(float angle)
{
	rotation = angle;

	UpdateTransform();
}

Vector2 Transform::GetPosition() const
{
	//Vector2 pos = Vector2(globalMatrix.m[0][2], globalMatrix.m[1][2]);
	//return pos;
	return position;
}

Vector2 Transform::GetScale() const
{
	return scale;
}

float Transform::GetRotation() const
{
	return rotation;
}

Matrix3 Transform::GetLocalMatrix() const
{
	return localMatrix;
}

Matrix3 Transform::GetGlobalMatrix() const
{
	if (parent != nullptr)
	{
		return parent->GetGlobalMatrix() * localMatrix;
	}
	else
	{
		return localMatrix;
	}
}

void Transform::SetParent(Transform * a_parent)
{
	parent = a_parent;
}

Transform * Transform::GetParent() const
{
	return parent;
}
