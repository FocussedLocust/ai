#pragma once
#include "Matrix3.h"
#include "Vector2.h"

class Transform
{
	Vector2 position;
	float rotation;
	Vector2 scale;

	//matrices
	Matrix3 translationMat;
	Matrix3 rotationMat;
	Matrix3 scaleMat;

	//
	Matrix3 localMatrix = translationMat * rotationMat * scaleMat;
	Matrix3 globalMatrix;

	//parent object
	Transform* parent;

public:
	Transform();
	~Transform();

	void UpdateTransform();

	//Move the object by this vector
	void Translate(Vector2);

	//scale the object along the x and y axis
	void Scale(float x, float y);

	//rotate the object on the z axis
	void Rotate(float angle);

	//set the object's position to be at these global values
	void SetPosition(float x, float y);

	//set the scale of the object to be these values
	void SetScale(float x, float y);

	//set the absolute rotation of this object  
	void setRotation(float angle);

	//returns the position, scale or rotation of the object
	Vector2 GetPosition() const;
	Vector2 GetScale() const;
	float GetRotation() const;

	//returns the local or global matrix
	Matrix3 GetLocalMatrix() const;
	Matrix3 GetGlobalMatrix() const;

	//set the object parent
	void SetParent(Transform* a_parent);
	Transform* GetParent() const;

	//true if the object has been changed
	bool isDirty;
};

