#pragma once
#include "IBehaviour.h"


class WanderBehaviour : public IBehaviour
{
public:
	WanderBehaviour();
	~WanderBehaviour();

	void Update(float deltaTime, Agent *pAgent);
};

