#include "Application2D.h"

int main() {
	
	int width = 1280;
	int height = 720;

	auto app = new Application2D(height, width);
	app->run("AIE", width, height, false);
	delete app;

	return 0;
}