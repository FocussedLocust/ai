#include "sprite.h"
#include "Transform.h"
#include "Input.h"

sprite::sprite()
{
}

sprite::sprite(aie::Texture* a_Texture, float x_pos, float y_pos, Vector2 a_Tex)
{
	m_Texture = a_Texture;

	playerTransform = Transform();
	playerTransform.SetPosition(x_pos, y_pos);

	Tex = a_Tex;
	radius = a_Tex.x;
	if (a_Tex.y > radius)
	{
		radius = a_Tex.y;
	}

	fileHandle = CreateFileMapping(
		INVALID_HANDLE_VALUE, // a handle to an existing virtual file, or invalid
		nullptr, //optional security attributes
		PAGE_READWRITE, // read/write access control
		0, sizeof(sprite), //size of the memory block
		L"MySharedMemory");

	data = (sprite*)MapViewOfFile(fileHandle, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(sprite));
}

sprite::~sprite()
{
	UnmapViewOfFile(data);

	CloseHandle(fileHandle);

}

void sprite::Update(float deltaTime)
{

	float drag = 2.0f;
	int speed = 100;
	int mass = 1;

	Vector2 direction = Vector2(0, 0);

	// input example
	aie::Input* input = aie::Input::getInstance();

	// use arrow keys to move player

	if (input->isKeyDown(aie::INPUT_KEY_UP))
	{

		Vector2 upaxis = Vector2(playerTransform.GetGlobalMatrix().m[1][0], playerTransform.GetGlobalMatrix().m[1][1]);
		direction = Vector2(upaxis.x, upaxis.y);
	}

	if (input->isKeyDown(aie::INPUT_KEY_DOWN))
	{

	}

	if (input->isKeyDown(aie::INPUT_KEY_LEFT))
	{
		playerTransform.Rotate(2.0f * deltaTime);
		data->playerTransform.Rotate(2.0f * deltaTime);
	}

	if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
	{
		playerTransform.Rotate(-2.0f  * deltaTime);
		data->playerTransform.Rotate(-2.0f  * deltaTime);
	}

	Vector2 forceSum = direction * speed;

	Vector2 acceleration = forceSum * (1.0f / mass);

	Vector2 dampening = (velocity * drag) * deltaTime;

	velocity += (acceleration - dampening);
	playerTransform.Translate(velocity * deltaTime);
	data->playerTransform.Translate(velocity * deltaTime);
}

void sprite::Draw(aie::Renderer2D & m_2dRenderer)
{

	m_2dRenderer.drawSpriteTransformed3x3(m_Texture, (float*)(playerTransform.GetGlobalMatrix()));

}
